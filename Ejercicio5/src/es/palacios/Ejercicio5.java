package es.palacios;

import java.io.*;

public class Ejercicio5 {

    final static String TOTALS_FILE_NAME = "resources/totals.txt";

    public static void main(String[] args) {
        final String adderJarPath = "out/artifacts/Ex5_Adder_jar/Ex5_Adder.jar";

        if (args.length == 0) {
            System.err.println("Necesito archivos para contar");
            System.exit(1);
        }

        int resultTotal = 0;
        FileWriter fw = null;
        try {
            fw = new FileWriter(TOTALS_FILE_NAME);
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(3);
        }
        for (String filePath : args) {
            Process process = null;
            try {
                process = new ProcessBuilder("java", "-jar", adderJarPath, filePath).start();
            } catch (IOException e) {
                System.err.println("IO Exception: " + e.getMessage());
                System.exit(4);
            }

            InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());

            try (BufferedReader bufReader = new BufferedReader(inputStreamReader);) {

                System.out.println("=========== CONTADOR DE NUMEROS ==========");

                String result = bufReader.readLine();
                resultTotal = resultTotal + Integer.parseInt(result);
                fw.write(result + System.lineSeparator());
                fw.flush();

            } catch (IOException e) {
                System.err.println("IO Exception: " + e.getMessage());
                System.exit(5);
            } finally {
                int exitValue = 0;
                try {
                    exitValue = process.waitFor();
                } catch (InterruptedException e) {
                    System.err.println("InterruptedException: " + e.getMessage());
                    System.exit(6);
                }
                System.out.println("La ejecución del programa Adder devuelve el valor de salida: " + exitValue);
                if (exitValue != 0) {
                    System.err.println("ERROR ADDER:");
                    printChildError(process.getErrorStream());
                }
            }
        }
        try {
            fw.write(String.valueOf(resultTotal));
            fw.flush();
            System.out.println("La suma total de los numeros encontrados en los diferentes archivvos es: " + resultTotal);
            System.out.println("Cada resultado y la suma total se encuntran en el siguiente archivo: " + TOTALS_FILE_NAME);
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(7);
        }
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream del mismo que recoge el error producido
     * Luego lo muestra por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(errorStream));
        String line;
        try {
            while ((line = bufReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
