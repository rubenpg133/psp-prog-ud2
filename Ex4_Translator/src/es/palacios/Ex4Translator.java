package es.palacios;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class Ex4Translator {
    public static void main(String[] args) {
        Dictionary dictionary;
        if (args.length == 0) {
            dictionary = new Dictionary();
        } else {
            dictionary = new Dictionary(new File(args[0]));
        }
        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in))) {
            String word;
            while ((word = bufReader.readLine()) != null && !word.equalsIgnoreCase("Fin")) {
                String translation;
                translation = dictionary.getTranslation(word);
                if (translation != null) {
                    System.out.println("Español: " + word + " --> Ingles: " + translation);
                } else {
                    System.out.println("Desconocido");
                }
            }

        } catch (Exception e) {
            System.err.println("Error en el proceso hijo Ex4Translator: " + e.getMessage());
            System.exit(1);
        }
    }

}
