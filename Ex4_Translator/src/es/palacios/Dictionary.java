package es.palacios;

import java.io.*;
import java.util.HashMap;

public class Dictionary {

    private HashMap<String, String> content;

    public Dictionary() {
        loadContent();
    }

    public Dictionary(File file){
        content = loadContentFile(file);
    }

    public String getTranslation(String word) {
        if (content.containsKey(word)) {
            return content.get(word);
        }
        return null;
    }

    /***
     * Este metodo rellena el HashMap de la clase Dictionary llamado "content"
     */
    private void loadContent() {
        content = new HashMap<>();
        content.put("gato", "cat");
        content.put("perro", "dog");
        content.put("leon", "lion");
        content.put("tigre", "tiger");
        content.put("negro", "black");
        content.put("blanco", "white");
        content.put("amarillo", "yellow");
        content.put("verde", "green");
        content.put("rojo", "red");
        content.put("naranja", "orange");
        content.put("violeta", "purple");
        content.put("rosa", "pink");
        content.put("lapiz", "pencil");
        content.put("teclado", "keyboard");
        content.put("ordenador", "computer");
        content.put("raton", "mouse");
        content.put("monitor", "monitor");
        content.put("television", "tv");
        content.put("aparato", "device");
        content.put("electrodoméstico", "appliance");
    }

    /***
     * Este metodo recibe un fichero, lo lee y añade su contenido a un HashMap
     * @param file contiene los elementos necesarios para crear el HashMap
     * @return devuleve un HashMap de String como key y String como Value
     */
    public HashMap<String,String> loadContentFile(File file) {
        HashMap<String, String> map = new HashMap<String, String>();
        String line;
        try(BufferedReader bufReader = new BufferedReader(new FileReader(file));){
            while ((line = bufReader.readLine()) != null)
            {
                String[] parts = line.split(":", 2);
                if (parts.length >= 2)
                {
                    String key = parts[0];
                    String value = parts[1];
                    map.put(key, value);
                }
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return map;
    }

}
