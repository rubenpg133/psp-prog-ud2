package es.palacios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Ex5Adder {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("Necesito un archivo para contar");
            System.exit(1);
        } else {
            File file = new File(args[0]);
            try {
                int total = 0;
                String number;
                BufferedReader bufReader = new BufferedReader(new FileReader(file));
                while ((number = bufReader.readLine()) != null) {
                    int numberToAdd = Integer.parseInt(number);
                    total = total + numberToAdd;
                }
                System.out.println(total);
            } catch (Exception e) {
                System.err.println("Error en el proceso hijo Ex5Adder: " + e.getMessage());
                System.exit(2);
            }
        }
    }
}
