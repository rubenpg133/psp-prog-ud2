##[Repositorio de Gitlab](https://gitlab.com/rubenpg133/psp-prog-ud2)

###Ex4-Translator

El ejecutable se encuentra en el siguiente [enlace](out/artifacts/Ex4_Translator_jar/Ex4_Translator.jar)

Puedes ejecutarlo sin parametros o pasandole un archivo que contenga un diccionario.

###Ejercicio4

El ejecutable se encuentra en el siguiente [enlace](out/artifacts/Ejercicio4_jar/Ejercicio4.jar)

Puedes ejecutarlo sin parametros, pasandole un archivo que contenga un diccionario o un archivo con palabras a traducir o las 2 cosas.
El archivo del diccionario debe incluir por lo menos los caracteres "dic" en su nombre para poder funcionar.

###Ex5-Adder

El ejecutable se encuentra en el siguiente [enlace](out/artifacts/Ex5_Adder_jar/Ex5_Adder.jar)

Puedes ejecutarlo pasandole un archivo que contenga numeros en cada linea.

###Ejercicio5

El ejecutable se encuentra en el siguiente [enlace](out/artifacts/Ejercicio5_jar/Ejercicio5.jar)

Puedes ejecutarlo pasandole varios archivos que contenga numeros en cada linea.
