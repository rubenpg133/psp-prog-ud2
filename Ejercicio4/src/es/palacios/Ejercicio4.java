package es.palacios;

import java.io.*;

public class Ejercicio4 {

    final static String TRANSLATIONS_FILE_NAME = "resources/translations.txt";

    public static void main(String[] args) {
        final String translatorJarPath = "out/artifacts/Ex4_Translator_jar/Ex4_Translator.jar";

        File wordsToTranslate = null;
        Process process = null;
        try {
            if (args.length == 0) {
                process = new ProcessBuilder("java", "-jar", translatorJarPath).start();
            } else if (args.length == 1){
                if (args[0].contains("dic")) {
                    String argumento = args[0];
                    process = new ProcessBuilder("java", "-jar", translatorJarPath, argumento).start();
                } else {
                    process = new ProcessBuilder("java", "-jar", translatorJarPath).start();
                    wordsToTranslate = new File(args[0]);
                }
            } else if (args.length == 2){
                if (args[0].contains("dic")) {
                    String argumento = args[0];
                    process = new ProcessBuilder("java", "-jar", translatorJarPath, argumento).start();
                    wordsToTranslate = new File(args[1]);
                } else {
                    String argumento = args[1];
                    process = new ProcessBuilder("java", "-jar", translatorJarPath, argumento).start();
                    wordsToTranslate = new File(args[0]);
                }
            }

        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(1);
        }

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
        InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());

        try (BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
             BufferedReader bufReader = new BufferedReader(inputStreamReader);
             FileWriter fw = new FileWriter(TRANSLATIONS_FILE_NAME)) {

            BufferedReader bufReaderWords = null;
            if (wordsToTranslate != null) {
                bufReaderWords = new BufferedReader(new FileReader(wordsToTranslate));
            } else {
                bufReaderWords = new BufferedReader(new InputStreamReader(System.in));
            }
            System.out.println("=========== TRADUCTOR ==========");

            String line;
            boolean goOn;
            do {
                System.out.print("Escribe una palabra en ESPAÑOL para traducirla al INGLES " +
                        "(Escribe 'Fin' para detener el programa): ");
                line = bufReaderWords.readLine();

                bufWriter.write(line);
                bufWriter.newLine();

                bufWriter.flush();

                goOn = !line.equalsIgnoreCase("Fin");
                if (goOn) {
                    String result = bufReader.readLine();
                    System.out.println(result);
                    fw.write(result + System.lineSeparator());
                }
            } while (goOn);
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(2);
        } finally {
            int exitValue = 0;
            try {
                exitValue = process.waitFor();
            } catch (InterruptedException e) {
                System.err.println("InterruptedException: " + e.getMessage());
                System.exit(3);
            }
            System.out.println(System.lineSeparator() + "La ejecución del programa Translator devuelve el valor de salida: " + exitValue);
            if (exitValue != 0) {
                System.err.println("ERROR TRANSLATOR:");
                printChildError(process.getErrorStream());
            }
        }
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream del mismo que recoge el error producido
     * Luego lo muestra por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(errorStream));
        String line;
        try {
            while ((line = bufReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
